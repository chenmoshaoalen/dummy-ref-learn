#include "6dof_kinematic.h"

inline float cosf(float x)
{
    return arm_cos_f32(x);
}

inline float sinf(float x)
{
    return arm_sin_f32(x);
}

/**
 * 矩阵相乘
 * @param _matrix1 左边矩阵
 * @param _matrix2 右边矩阵
 * @param _matrixOut 输出
 * @param _m
 * @param _l
 * @param _n
 */
static void MatMultiply(const float* _matrix1, const float* _matrix2, float* _matrixOut,
                        const int _m, const int _l, const int _n)
{
    float tmp;
    int i, j, k;
    for (i = 0; i < _m; i++)
    {
        for (j = 0; j < _n; j++)
        {
            tmp = 0.0f;
            for (k = 0; k < _l; k++)
            {
                tmp += _matrix1[_l * i + k] * _matrix2[_n * k + j];
            }
            _matrixOut[_n * i + j] = tmp;
        }
    }
}

/**
 * 旋转矩阵转ZYX欧拉角(XYZ固定角）
 * @param _rotationM
 * @param _eulerAngles
 */
static void RotMatToEulerAngle(const float* _rotationM, float* _eulerAngles)
{
    float A, B, C, cb;

    if (fabs(_rotationM[6]) >= 1.0 - 0.0001)
    {
        if (_rotationM[6] < 0)
        {
            A = 0.0f;
            B = (float) M_PI_2;
            C = atan2f(_rotationM[1], _rotationM[4]);
        } else
        {
            A = 0.0f;
            B = -(float) M_PI_2;
            C = -atan2f(_rotationM[1], _rotationM[4]);
        }
    } else
    {
        B = atan2f(-_rotationM[6], sqrtf(_rotationM[0] * _rotationM[0] + _rotationM[3] * _rotationM[3]));
        cb = cosf(B);
        A = atan2f(_rotationM[3] / cb, _rotationM[0] / cb);
        C = atan2f(_rotationM[7] / cb, _rotationM[8] / cb);
    }

    _eulerAngles[0] = C;
    _eulerAngles[1] = B;
    _eulerAngles[2] = A;
}

/**
 * ZYX欧拉角（XYZ固定角）转旋转矩阵
 * @param _eulerAngles
 * @param _rotationM
 */
static void EulerAngleToRotMat(const float* _eulerAngles, float* _rotationM)
{
    float ca, cb, cc, sa, sb, sc;

    cc = cosf(_eulerAngles[0]);
    cb = cosf(_eulerAngles[1]);
    ca = cosf(_eulerAngles[2]);
    sc = sinf(_eulerAngles[0]);
    sb = sinf(_eulerAngles[1]);
    sa = sinf(_eulerAngles[2]);

    _rotationM[0] = ca * cb;
    _rotationM[1] = ca * sb * sc - sa * cc;
    _rotationM[2] = ca * sb * cc + sa * sc;
    _rotationM[3] = sa * cb;
    _rotationM[4] = sa * sb * sc + ca * cc;
    _rotationM[5] = sa * sb * cc - ca * sc;
    _rotationM[6] = -sb;
    _rotationM[7] = cb * sc;
    _rotationM[8] = cb * cc;
}


/**
 *
 * @param 以下是DH参数
 * @param L_BS
 * @param D_BS
 * @param L_AM
 * @param L_FA
 * @param D_EW
 * @param L_WT
 */
DOF6Kinematic::DOF6Kinematic(float L_BS, float D_BS, float L_AM, float L_FA, float D_EW, float L_WT)
    : armConfig(ArmConfig_t{L_BS, D_BS, L_AM, L_FA, D_EW, L_WT})
{
    float tmp_DH_matrix[6][4] = {
        {0.0f,            armConfig.L_BASE,    armConfig.D_BASE, -(float) M_PI_2},
        {-(float) M_PI_2, 0.0f,                armConfig.L_ARM,  0.0f},
        {(float) M_PI_2,  armConfig.D_ELBOW,   0.0f,             (float) M_PI_2},
        {0.0f,            armConfig.L_FOREARM, 0.0f,             -(float) M_PI_2},
        {0.0f,            0.0f,                0.0f,             (float) M_PI_2},
        {0.0f,            armConfig.L_WRIST, 0.0f, 0.0f}
    };
    memcpy(DH_matrix, tmp_DH_matrix, sizeof(tmp_DH_matrix));

    float tmp_L1_bs[3] = {armConfig.D_BASE, -armConfig.L_BASE, 0.0f};
    memcpy(L1_base, tmp_L1_bs, sizeof(tmp_L1_bs));
    float tmp_L2_se[3] = {armConfig.L_ARM, 0.0f, 0.0f};
    memcpy(L2_arm, tmp_L2_se, sizeof(tmp_L2_se));
    float tmp_L3_ew[3] = {-armConfig.D_ELBOW, 0.0f, armConfig.L_FOREARM};
    memcpy(L3_elbow, tmp_L3_ew, sizeof(tmp_L3_ew));
    float tmp_L6_wt[3] = {0.0f, 0.0f, armConfig.L_WRIST};
    memcpy(L6_wrist, tmp_L6_wt, sizeof(tmp_L6_wt));

    l_se_2 = armConfig.L_ARM * armConfig.L_ARM;
    l_se = armConfig.L_ARM;
    l_ew_2 = armConfig.L_FOREARM * armConfig.L_FOREARM + armConfig.D_ELBOW * armConfig.D_ELBOW;
    l_ew = 0;
    atan_e = 0;
}

/**
 * 正向运动学解算
 * @param _inputJoint6D 关节位置
 * @param _outputPose6D 末端姿态
 * @return  计算完成标志
 */
bool
DOF6Kinematic::SolveFK(const DOF6Kinematic::Joint6D_t &_inputJoint6D, DOF6Kinematic::Pose6D_t &_outputPose6D)
{
    float q_in[6];
    float q[6];
    float cosq, sinq;
    float cosa, sina;
    float P06[6];
    float R06[9];
    float R[6][9];
    float R02[9];
    float R03[9];
    float R04[9];
    float R05[9];
    float L0_bs[3];
    float L0_se[3];
    float L0_ew[3];
    float L0_wt[3];

//    转单位为rad
    for (int i = 0; i < 6; i++)
        q_in[i] = _inputJoint6D.a[i] / RAD_TO_DEG;

    for (int i = 0; i < 6; i++)
    {
        q[i] = q_in[i] + DH_matrix[i][0];
        cosq = cosf(q[i]);
        sinq = sinf(q[i]);
        cosa = cosf(DH_matrix[i][3]);
        sina = sinf(DH_matrix[i][3]);

        R[i][0] = cosq;
        R[i][1] = -cosa * sinq;
        R[i][2] = sina * sinq;
        R[i][3] = sinq;
        R[i][4] = cosa * cosq;
        R[i][5] = -sina * cosq;
        R[i][6] = 0.0f;
        R[i][7] = sina;
        R[i][8] = cosa;
    }

    MatMultiply(R[0], R[1], R02, 3, 3, 3);
    MatMultiply(R02, R[2], R03, 3, 3, 3);
    MatMultiply(R03, R[3], R04, 3, 3, 3);
    MatMultiply(R04, R[4], R05, 3, 3, 3);
    MatMultiply(R05, R[5], R06, 3, 3, 3);

    MatMultiply(R[0], L1_base, L0_bs, 3, 3, 1);
    MatMultiply(R02, L2_arm, L0_se, 3, 3, 1);
    MatMultiply(R03, L3_elbow, L0_ew, 3, 3, 1);
    MatMultiply(R06, L6_wrist, L0_wt, 3, 3, 1);

    for (int i = 0; i < 3; i++)
        P06[i] = L0_bs[i] + L0_se[i] + L0_ew[i] + L0_wt[i];

    RotMatToEulerAngle(R06, &(P06[3]));

    _outputPose6D.X = P06[0];
    _outputPose6D.Y = P06[1];
    _outputPose6D.Z = P06[2];
    _outputPose6D.A = P06[3] * RAD_TO_DEG;
    _outputPose6D.B = P06[4] * RAD_TO_DEG;
    _outputPose6D.C = P06[5] * RAD_TO_DEG;
    memcpy(_outputPose6D.R, R06, 9 * sizeof(float));

    return true;
}

/**
 * 逆运动学解算，实在没法看，根据模型和解说视频，推测使用Pieper准则解算的，根据上次的关节位置处理多解
 * @param _inputPose6D 末端位姿
 * @param _lastJoint6D 上一次的关节角度
 * @param _outputSolves
 * @return
 */
bool DOF6Kinematic::SolveIK(const DOF6Kinematic::Pose6D_t &_inputPose6D, const Joint6D_t &_lastJoint6D,
                            DOF6Kinematic::IKSolves_t &_outputSolves)
{
    float qs[2];// 肩部关节角度
    float qa[2][2];// 肘部关节角度
    float qw[2][3];// 腕部关节角度
    float cosqs, sinqs;// 肩部关节的余弦和正弦值
    float cosqa[2], sinqa[2];// 肘部关节的余弦和正弦值
    float cosqw, sinqw;// 腕部关节的余弦和正弦值
    float P06[6];// 输入姿态的6个参数（X, Y, Z, A, B, C）
    float R06[9];// 旋转矩阵，用于姿态转换
    float P0_w[3];// 用于计算的中间点
    float P1_w[3];// 另一个用于计算的中间点
    float L0_wt[3];// 中间变量
    float L1_sw[3];// 中间变量
    float R10[9];// 旋转矩阵，用于计算
    float R31[9];// 旋转矩阵，用于计算
    float R30[9];// 旋转矩阵，用于计算
    float R36[9];// 旋转矩阵，用于计算
    float l_sw_2, l_sw, atan_a, acos_a, acos_e;// 用于几何计算的中间变量

    int ind_arm, ind_elbow, ind_wrist;// 用于循环的索引
    int i;// 用于循环的索引

    if (0 == l_ew)// 检查elbow-wrist长度是否已定义，若未定义则计算它
    {
        l_ew = sqrtf(l_ew_2);// 计算elbow-wrist长度
        atan_e = atanf(armConfig.D_ELBOW / armConfig.L_FOREARM);// 计算肘关节的角度
    }
    // 将输入的位置转换为内部表示，单位从毫米转换为米
    P06[0] = _inputPose6D.X / 1000.0f;
    P06[1] = _inputPose6D.Y / 1000.0f;
    P06[2] = _inputPose6D.Z / 1000.0f;
    if (!_inputPose6D.hasR)// 检查输入姿态是否包含旋转矩阵
    {
        // 如果没有旋转矩阵，则从欧拉角转换
        P06[3] = _inputPose6D.A / RAD_TO_DEG;
        P06[4] = _inputPose6D.B / RAD_TO_DEG;
        P06[5] = _inputPose6D.C / RAD_TO_DEG;
        EulerAngleToRotMat(&(P06[3]), R06);
    } else
    {
        // 如果有旋转矩阵，则直接使用
        memcpy(R06, _inputPose6D.R, 9 * sizeof(float));// 直接复制旋转矩阵
    }
    for (i = 0; i < 2; i++)// 初始化关节角度为上一次的值
    {
        qs[i] = _lastJoint6D.a[0];
        qa[i][0] = _lastJoint6D.a[1];
        qa[i][1] = _lastJoint6D.a[2];
        qw[i][0] = _lastJoint6D.a[3];
        qw[i][1] = _lastJoint6D.a[4];
        qw[i][2] = _lastJoint6D.a[5];
    }
    MatMultiply(R06, L6_wrist, L0_wt, 3, 3, 1);// 使用旋转矩阵和位姿计算L0_wt
    for (i = 0; i < 3; i++)// 从位置中减去计算得到的中间值
    {
        P0_w[i] = P06[i] - L0_wt[i];
    }
    // 检查P0_w的值并进行处理
    if (sqrt(P0_w[0] * P0_w[0] + P0_w[1] * P0_w[1]) <= 0.000001)
    {
        // 如果值非常小，则保持原始肩部关节角度
        qs[0] = _lastJoint6D.a[0];
        qs[1] = _lastJoint6D.a[0];
        for (i = 0; i < 4; i++)// 标记为无解
        {
            _outputSolves.solFlag[0 + i][0] = -1;
            _outputSolves.solFlag[4 + i][0] = -1;
        }
    } else
    {
        // 计算qs的两个可能值
        qs[0] = atan2f(P0_w[1], P0_w[0]);// 计算第一个可能的肩部角度
        qs[1] = atan2f(-P0_w[1], -P0_w[0]);// 计算第二个可能的肩部角度
        for (i = 0; i < 4; i++)// 标记这个解为有效
        {
            _outputSolves.solFlag[0 + i][0] = 1;
            _outputSolves.solFlag[4 + i][0] = 1;
        }
    }
    // 遍历可能的肩部解
    for (ind_arm = 0; ind_arm < 2; ind_arm++)
    {
        // 计算肩部关节角的余弦和正弦值
        cosqs = cosf(qs[ind_arm] + DH_matrix[0][0]);
        sinqs = sinf(qs[ind_arm] + DH_matrix[0][0]);

        // 构建旋转矩阵R10
        R10[0] = cosqs;
        R10[1] = sinqs;
        R10[2] = 0.0f;
        R10[3] = 0.0f;
        R10[4] = 0.0f;
        R10[5] = -1.0f;
        R10[6] = -sinqs;
        R10[7] = cosqs;
        R10[8] = 0.0f;

        MatMultiply(R10, P0_w, P1_w, 3, 3, 1);// 使用R10和P0_w计算P1_w
        for (i = 0; i < 3; i++)// 计算L1_sw
        {
            L1_sw[i] = P1_w[i] - L1_base[i];
        }
        l_sw_2 = L1_sw[0] * L1_sw[0] + L1_sw[1] * L1_sw[1];// 计算l_sw的平方
        l_sw = sqrtf(l_sw_2);// 计算l_sw的模
        // 检查并处理特定的几何条件
        if (fabs(l_se + l_ew - l_sw) <= 0.000001)
        {
            // 当肘部和腕部长度之和接近l_sw时
            qa[0][0] = atan2f(L1_sw[1], L1_sw[0]);// 计算第一种肘部解
            qa[1][0] = qa[0][0];// 第二种肘部解与第一种相同
            qa[0][1] = 0.0f;// 肘部的第二个关节角
            qa[1][1] = 0.0f;// 肘部的第二个关节角
            // 根据l_sw和l_se + l_ew的关系设置解的有效性
            if (l_sw > l_se + l_ew)
            {
                for (i = 0; i < 2; i++)// 标记为无解
                {
                    _outputSolves.solFlag[4 * ind_arm + 0 + i][1] = 0;
                    _outputSolves.solFlag[4 * ind_arm + 2 + i][1] = 0;
                }
            } else
            {
                for (i = 0; i < 2; i++)// 标记为有效解
                {
                    _outputSolves.solFlag[4 * ind_arm + 0 + i][1] = 1;
                    _outputSolves.solFlag[4 * ind_arm + 2 + i][1] = 1;
                }
            }
        } else if (fabs(l_sw - fabs(l_se - l_ew)) <= 0.000001)
        {
            // 当l_sw接近l_se和l_ew的差值时
            qa[0][0] = atan2f(L1_sw[1], L1_sw[0]);// 计算第一种肘部解
            qa[1][0] = qa[0][0];// 第二种肘部解与第一种相同

            // 根据ind_arm的值设置肘部的第二个关节角
            if (0 == ind_arm)
            {
                qa[0][1] = (float) M_PI;
                qa[1][1] = -(float) M_PI;
            } else
            {
                qa[0][1] = -(float) M_PI;
                qa[1][1] = (float) M_PI;
            }

            // 根据l_sw和l_se - l_ew的关系设置解的有效性
            if (l_sw < fabs(l_se - l_ew))
            {
                for (i = 0; i < 2; i++)
                {
                    _outputSolves.solFlag[4 * ind_arm + 0 + i][1] = 0;
                    _outputSolves.solFlag[4 * ind_arm + 2 + i][1] = 0;
                }
            } else
            {
                for (i = 0; i < 2; i++)
                {
                    _outputSolves.solFlag[4 * ind_arm + 0 + i][1] = 1;
                    _outputSolves.solFlag[4 * ind_arm + 2 + i][1] = 1;
                }
            }
        } else
        {
            // 对于其他情况，进行更复杂的计算
            atan_a = atan2f(L1_sw[1], L1_sw[0]);
            acos_a = 0.5f * (l_se_2 + l_sw_2 - l_ew_2) / (l_se * l_sw);
            if (acos_a >= 1.0f) acos_a = 0.0f;
            else if (acos_a <= -1.0f) acos_a = (float) M_PI;
            else acos_a = acosf(acos_a);
            acos_e = 0.5f * (l_se_2 + l_ew_2 - l_sw_2) / (l_se * l_ew);
            if (acos_e >= 1.0f) acos_e = 0.0f;
            else if (acos_e <= -1.0f) acos_e = (float) M_PI;
            else acos_e = acosf(acos_e);

            if (0 == ind_arm)// 根据ind_arm的值设置肘部角度
            {
                qa[0][0] = atan_a - acos_a + (float) M_PI_2;// 计算第一种肘部解
                qa[0][1] = atan_e - acos_e + (float) M_PI;// 计算第一种肘部解的第二个角度
                qa[1][0] = atan_a + acos_a + (float) M_PI_2;// 计算第二种肘部解
                qa[1][1] = atan_e + acos_e - (float) M_PI;// 计算第二种肘部解的第二个角度

            } else
            {
                qa[0][0] = atan_a + acos_a + (float) M_PI_2;
                qa[0][1] = atan_e + acos_e - (float) M_PI;
                qa[1][0] = atan_a - acos_a + (float) M_PI_2;
                qa[1][1] = atan_e - acos_e + (float) M_PI;
            }

            for (i = 0; i < 2; i++)// 标记为有效解
            {
                _outputSolves.solFlag[4 * ind_arm + 0 + i][1] = 1;
                _outputSolves.solFlag[4 * ind_arm + 2 + i][1] = 1;
            }
        }

        for (ind_elbow = 0; ind_elbow < 2; ind_elbow++)// 遍历肘部可能的两个解
        {
            // 计算肘部关节的余弦和正弦值
            cosqa[0] = cosf(qa[ind_elbow][0] + DH_matrix[1][0]);
            sinqa[0] = sinf(qa[ind_elbow][0] + DH_matrix[1][0]);
            cosqa[1] = cosf(qa[ind_elbow][1] + DH_matrix[2][0]);
            sinqa[1] = sinf(qa[ind_elbow][1] + DH_matrix[2][0]);

            R31[0] = cosqa[0] * cosqa[1] - sinqa[0] * sinqa[1];
            R31[1] = cosqa[0] * sinqa[1] + sinqa[0] * cosqa[1];
            R31[2] = 0.0f;
            R31[3] = 0.0f;
            R31[4] = 0.0f;
            R31[5] = 1.0f;
            R31[6] = cosqa[0] * sinqa[1] + sinqa[0] * cosqa[1];
            R31[7] = -cosqa[0] * cosqa[1] + sinqa[0] * sinqa[1];
            R31[8] = 0.0f;

            // 使用R31, R10和R06计算R30和R36
            MatMultiply(R31, R10, R30, 3, 3, 3);
            MatMultiply(R30, R06, R36, 3, 3, 3);

            // 检查R36[8]的值并根据其值计算腕部关节角
            if (R36[8] >= 1.0 - 0.000001)
            {
                // 如果R36[8]接近1，表示特殊情况
                cosqw = 1.0f;
                qw[0][1] = 0.0f;
                qw[1][1] = 0.0f;
            } else if (R36[8] <= -1.0 + 0.000001)
            {
                // 如果R36[8]接近-1，表示另一种特殊情况
                cosqw = -1.0f;
                if (0 == ind_arm)
                {
                    qw[0][1] = (float) M_PI;
                    qw[1][1] = -(float) M_PI;
                } else
                {
                    qw[0][1] = -(float) M_PI;
                    qw[1][1] = (float) M_PI;
                }
            } else
            {
                // 一般情况下的计算
                cosqw = R36[8];
                // 根据ind_arm设置腕部角度
                if (0 == ind_arm)
                {
                    qw[0][1] = acosf(cosqw);
                    qw[1][1] = -acosf(cosqw);
                } else
                {
                    qw[0][1] = -acosf(cosqw);
                    qw[1][1] = acosf(cosqw);
                }
            }
            // 检查cosqw的值，以确定是否存在有效的腕部关节角
            if (1.0f == cosqw || -1.0f == cosqw)
            {
                // 特殊情况，一些腕部角度无法确定
                if (0 == ind_arm)
                {
                    qw[0][0] = _lastJoint6D.a[3];// 使用上一次的角度
                    cosqw = cosf(_lastJoint6D.a[3] + DH_matrix[3][0]);
                    sinqw = sinf(_lastJoint6D.a[3] + DH_matrix[3][0]);
                    qw[0][2] = atan2f(cosqw * R36[3] - sinqw * R36[0], cosqw * R36[0] + sinqw * R36[3]);
                    qw[1][2] = _lastJoint6D.a[5];// 使用上一次的角度
                    cosqw = cosf(_lastJoint6D.a[5] + DH_matrix[5][0]);
                    sinqw = sinf(_lastJoint6D.a[5] + DH_matrix[5][0]);
                    qw[1][0] = atan2f(cosqw * R36[3] - sinqw * R36[0], cosqw * R36[0] + sinqw * R36[3]);
                } else
                {
                    qw[0][2] = _lastJoint6D.a[5];// 使用上一次的角度
                    cosqw = cosf(_lastJoint6D.a[5] + DH_matrix[5][0]);
                    sinqw = sinf(_lastJoint6D.a[5] + DH_matrix[5][0]);
                    qw[0][0] = atan2f(cosqw * R36[3] - sinqw * R36[0], cosqw * R36[0] + sinqw * R36[3]);
                    qw[1][0] = _lastJoint6D.a[3];// 使用上一次的角度
                    cosqw = cosf(_lastJoint6D.a[3] + DH_matrix[3][0]);
                    sinqw = sinf(_lastJoint6D.a[3] + DH_matrix[3][0]);
                    qw[1][2] = atan2f(cosqw * R36[3] - sinqw * R36[0], cosqw * R36[0] + sinqw * R36[3]);
                }
                // 标记腕部关节角为无效
                _outputSolves.solFlag[4 * ind_arm + 2 * ind_elbow + 0][2] = -1;
                _outputSolves.solFlag[4 * ind_arm + 2 * ind_elbow + 1][2] = -1;
            } else
            {
                // 一般情况下计算腕部关节角
                if (0 == ind_arm)
                {
                    qw[0][0] = atan2f(R36[5], R36[2]);// 计算第一种解的第一个腕部角度
                    qw[1][0] = atan2f(-R36[5], -R36[2]);// 计算第二种解的第一个腕部角度
                    qw[0][2] = atan2f(R36[7], -R36[6]);// 计算第一种解的第三个腕部角度
                    qw[1][2] = atan2f(-R36[7], R36[6]);// 计算第二种解的第三个腕部角度
                } else
                {
                    qw[0][0] = atan2f(-R36[5], -R36[2]);
                    qw[1][0] = atan2f(R36[5], R36[2]);
                    qw[0][2] = atan2f(-R36[7], R36[6]);
                    qw[1][2] = atan2f(R36[7], -R36[6]);
                }
                // 标记这些腕部关节角为有效
                _outputSolves.solFlag[4 * ind_arm + 2 * ind_elbow + 0][2] = 1;
                _outputSolves.solFlag[4 * ind_arm + 2 * ind_elbow + 1][2] = 1;
            }
            // 遍历腕部可能的两个解
            for (ind_wrist = 0; ind_wrist < 2; ind_wrist++)
            {
                // 调整qs角度，使其位于[-π, π]区间
                if (qs[ind_arm] > (float) M_PI)
                    _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[0] =
                        qs[ind_arm] - (float) M_PI;
                else if (qs[ind_arm] < -(float) M_PI)
                    _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[0] =
                        qs[ind_arm] + (float) M_PI;
                else
                    _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[0] = qs[ind_arm];
                // 对肘部角度做同样的调整,使其位于[-π, π]区间
                for (i = 0; i < 2; i++)
                {
                    if (qa[ind_elbow][i] > (float) M_PI)
                        _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[1 + i] =
                            qa[ind_elbow][i] - (float) M_PI;
                    else if (qa[ind_elbow][i] < -(float) M_PI)
                        _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[1 + i] =
                            qa[ind_elbow][i] + (float) M_PI;
                    else
                        _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[1 + i] =
                            qa[ind_elbow][i];
                }
                // 对腕部角度做同样的调整,使其位于[-π, π]区间
                for (i = 0; i < 3; i++)
                {
                    if (qw[ind_wrist][i] > (float) M_PI)
                        _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[3 + i] =
                            qw[ind_wrist][i] - (float) M_PI;
                    else if (qw[ind_wrist][i] < -(float) M_PI)
                        _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[3 + i] =
                            qw[ind_wrist][i] + (float) M_PI;
                    else
                        _outputSolves.config[4 * ind_arm + 2 * ind_elbow + ind_wrist].a[3 + i] =
                            qw[ind_wrist][i];
                }
            }
        }
    }

    // 转换所有解的角度为角度制
    for (i = 0; i < 8; i++)
        for (float &j: _outputSolves.config[i].a)
            j *= RAD_TO_DEG;

    return true;// 函数返回true，表示成功计算逆运动学解
}

DOF6Kinematic::Joint6D_t
operator-(const DOF6Kinematic::Joint6D_t &_joints1, const DOF6Kinematic::Joint6D_t &_joints2)
{
    DOF6Kinematic::Joint6D_t tmp{};
    for (int i = 0; i < 6; i++)
        tmp.a[i] = _joints1.a[i] - _joints2.a[i];

    return tmp;
}
